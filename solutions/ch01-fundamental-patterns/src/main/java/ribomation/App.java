package ribomation;

import ribomation.number.FlexiNumber;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    void run() {
        var a = new FlexiNumber(42);
        var b = new FlexiNumber(3.1415);
        var c = a.add(b);
        System.out.printf("%.2f + %.2f = %.2f%n",
                a.value(), b.value(), c.value());
    }
}
