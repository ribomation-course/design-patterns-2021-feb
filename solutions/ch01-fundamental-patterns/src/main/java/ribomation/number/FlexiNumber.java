package ribomation.number;

public class FlexiNumber {
    private final NumberImpl impl;

    public FlexiNumber(int value) {
        impl = new NumberImpl(value);
    }

    public FlexiNumber(double value) {
        impl = new NumberImpl(value);
    }

    private FlexiNumber(NumberImpl impl) {
        this.impl = impl;
    }

    public FlexiNumber add(FlexiNumber rhs) {
        NumberImpl impl;
        if (this.impl.type == NumberImpl.Type.INT && rhs.impl.type == NumberImpl.Type.INT) {
            impl = new NumberImpl(this.impl.intValue + rhs.impl.intValue);
        } else {
            impl = new NumberImpl(this.impl.floatValue + rhs.impl.floatValue);
        }
        return new FlexiNumber(impl);
    }

    public double value() {
        return (impl.type == NumberImpl.Type.INT) ? impl.intValue : impl.floatValue;
    }

}

