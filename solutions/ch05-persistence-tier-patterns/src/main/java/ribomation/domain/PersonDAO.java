package ribomation.domain;

import ribomation.dao.GenericDAO;

import java.nio.file.Path;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class PersonDAO extends GenericDAO<Person> {
    public PersonDAO(Path dbDir) {
        super(dbDir, Person.class);
    }

    public List<Person> findAllByName(String nameFragment) {
        final String phrase = nameFragment.toLowerCase();
        return findAll().stream()
                .filter(p -> p.getFirstName().toLowerCase().contains(phrase)
                        || p.getLastName().toLowerCase().contains(phrase))
                .collect(toList());
    }

    public List<Person> findAllByAge(int minAge, int maxAge) {
        return findAll().stream()
                .filter(p -> p.getAge() >= minAge)
                .filter(p -> p.getAge() <= maxAge)
                .collect(toList());
    }

}
