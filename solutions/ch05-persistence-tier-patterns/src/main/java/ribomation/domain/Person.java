package ribomation.domain;

import ribomation.dao.DomainClass;

public class Person extends DomainClass {
    private String firstName;
    private String lastName;
    private int age;
    private String email;
    private String city;

    public Person(String id) {
        super(id);
    }

    public Person(String id, String firstName, String lastName, int age, String email, String city) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.email = email;
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAge(String age) {
        setAge(Integer.parseInt(age));
    }

    @Override
    public String toString() {
        return String.format("Person{%s, %s %s, age %d <%s> @ %s}",
                getId(), getFirstName(), getLastName(), getAge(), getEmail(), getCity());
    }
}
