package ribomation.domain;

public class CsvMapper {

    public Person fromCsv(String id, String csv, String delim) {
        var f         = csv.split(delim);
        var ix        = 0;
        var firstName = f[ix++];
        var lastName  = f[ix++];
        var age       = Integer.parseInt(f[ix++]);
        var email     = f[ix++];
        var city      = f[ix++];
        return new Person(id, firstName, lastName, age, email, city);
    }

}
