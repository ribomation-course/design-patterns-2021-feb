package ribomation;

import ribomation.domain.CsvMapper;
import ribomation.domain.Person;
import ribomation.domain.PersonDAO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Objects;

public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        var dbDir = Path.of("./db");
        app.populate(dbDir);
        app.run(dbDir);
        app.cleanup(dbDir);
    }

    void populate(Path dbDir) throws IOException {
        if (!Files.exists(dbDir)) {
            Files.createDirectories(dbDir);
        }

        final PersonDAO dao = new PersonDAO(dbDir);
        final var in = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(
                                getClass().getResourceAsStream("/persons.csv"))));
        try (in) {
            final var mapper = new CsvMapper();
            final var nextId = new int[]{0};
            in.lines()
                    .skip(1)
                    .map(line -> mapper.fromCsv(String.format("obj%d", ++nextId[0]), line, ","))
                    .forEach(dao::save);
            System.out.printf("Populated DB (%s) with %d objects%n", dbDir, nextId[0]);
        }
    }

    void run(Path dbDir) {
        final PersonDAO dao = new PersonDAO(dbDir);

        System.out.println("-- persons of age [40, 45] --");
        dao.findAllByAge(40, 45)
                .stream()
                .sorted(Comparator.comparingInt(Person::getAge))
                .forEach(System.out::println);

        System.out.println("-- persons with name containing 'oo'");
        dao.findAllByName("oo")
                .stream()
                .sorted(Comparator.comparing(Person::getLastName))
                .forEach(System.out::println);
    }

    void cleanup(Path dbDir) {
        final PersonDAO dao = new PersonDAO(dbDir);
        dao.findAll().forEach(p -> dao.remove(p.getId()));
    }

}
