package ribomation.dao;

import java.util.List;
import java.util.Optional;

public interface DAO<Type extends DomainClass> {
    Optional<Type> findById(String id);

    List<Type> findAll();

    void save(Type obj);

    void remove(String id);
}
