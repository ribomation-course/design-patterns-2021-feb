package ribomation.dao;

import java.util.Properties;

public class PropertiesMapper<Type extends DomainClass> {
    private final Class clsType;

    public PropertiesMapper(Class clsType) {
        this.clsType = clsType;
    }

    public Type fromProps(String id, Properties data) {
        try {
            var constructor = clsType.getConstructor(String.class);
            var obj = constructor.newInstance(id);

            for (var e : data.entrySet()) {
                var name       = e.getKey().toString();
                var value      = e.getValue().toString();
                var methodName = "set" + name.substring(0, 1).toUpperCase() + name.substring(1);
                var setter     = clsType.getMethod(methodName, String.class);
                setter.invoke(obj, value);
            }

            return (Type) obj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Properties toProps(Type obj) {
        try {
            var data = new Properties();

            for (var getter : clsType.getDeclaredMethods()) {
                if (!getter.getName().startsWith("get")) continue;

                var name = getter.getName().substring(3);
                name = name.substring(0, 1).toLowerCase() + name.substring(1);
                var value = getter.invoke(obj).toString();

                data.setProperty(name, value);
            }

            return data;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
