package ribomation.dao;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class PropertiesMapperTest {

    @Test
    @DisplayName("a dummy object should transform into a props")
    void toProps() {
        var expected = new Properties();
        expected.setProperty("name", "Nisse");
        expected.setProperty("number", "42");

        var mapper = new PropertiesMapper<Dummy>(Dummy.class);
        var actual = mapper.toProps(new Dummy("aa", "Nisse", 42));

        assertEquals(expected, actual);
    }

    @Test
    void fromProps() {
        var data = new Properties();
        data.setProperty("name", "Nisse");
        data.setProperty("number", "42");

        var mapper = new PropertiesMapper<Dummy>(Dummy.class);
        var obj = mapper.fromProps("qq", data);

        assertEquals("qq", obj.getId());
        assertEquals("Nisse", obj.getName());
        assertEquals(42, obj.getNumber());
    }

}