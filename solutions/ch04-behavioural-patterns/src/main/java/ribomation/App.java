package ribomation;

import ribomation.laptop.*;
import ribomation.parts.Part;
import ribomation.visitors.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class App {
    public static void main(String[] args) throws IOException {
        new App().run();
    }

    void run() throws IOException {
        var tmpDir = Path.of("./tmp");
        Files.createDirectories(tmpDir);

        var pc = build();
        process(pc, new DumpVisitor(), Path.of(tmpDir.toString(), "pc.txt"));
        process(pc, new XmlVisitor(), Path.of(tmpDir.toString(), "pc.xml"));
        process(pc, new JsonVisitor(), Path.of(tmpDir.toString(), "pc.json"));
    }

    Part build() {
        return new Laptop("Dell Inspiron", "7773 2-in-1")
                .with(p -> {
                    p.add(new Screen("Philips BDM3470", 3440, 1440, 29));
                    p.add(new Keyboard("English GB-en"));
                    p.add(new Touchpad(105, 80));
                    p.add(new MotherBoard("Dell 0R58C3")
                            .with(mb -> {
                                mb.add(new CPU("Intel Core i7", 2.9));
                                mb.add(new Memory("DDR4 2400MHz", 16));
                                mb.add(new Fan("M3800 Cooling Fan"));
                            }));
                });
    }

    void process(Part pc, Visitor visitor, Path file) throws IOException {
        pc.accept(visitor);
        visitor.writeTo(file);
        System.out.printf("written %s (%d bytes)%n", file, Files.size(file));
    }
}
