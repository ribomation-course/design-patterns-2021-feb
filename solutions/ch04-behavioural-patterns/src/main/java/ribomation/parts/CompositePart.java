package ribomation.parts;

import ribomation.visitors.Visitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public abstract class CompositePart implements Part {
    private final List<Part> parts = new ArrayList<>();

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
        for (Part part : parts) part.accept(visitor);
        visitor.visitEnd(this);
    }

    @Override
    public Iterator<Part> iterator() {
        return parts.iterator();
    }

    @Override
    public void add(Part part) {
        parts.add(part);
    }

    public CompositePart with(Consumer<Part> composite) {
        composite.accept(this);
        return this;
    }
}


