package ribomation.visitors;

import ribomation.parts.CompositePart;
import ribomation.parts.Part;

import java.nio.file.Path;

public interface Visitor {
    void visit(CompositePart part);
    void visitEnd(CompositePart part);
    void visit(Part part);
    void writeTo(Path file);
}


