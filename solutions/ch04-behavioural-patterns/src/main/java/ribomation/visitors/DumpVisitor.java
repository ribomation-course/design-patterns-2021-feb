package ribomation.visitors;

import ribomation.laptop.*;
import ribomation.parts.CompositePart;
import ribomation.parts.Part;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;


public class DumpVisitor implements Visitor {
    int currentIndent = 0;
    StringWriter buf = new StringWriter(10000);
    PrintWriter out = new PrintWriter(buf, true);

    @Override
    public void visit(CompositePart part) {
        if (part instanceof Laptop) {
            var pc = (Laptop) part;
            printf("%s %s%n", pc.name, pc.model);
        } else if (part instanceof MotherBoard) {
            var mb = (MotherBoard) part;
            printf("Motherboard: %s%n", mb.name);
        }
        currentIndent++;
    }

    @Override
    public void visitEnd(CompositePart part) {
        currentIndent--;
    }

    @Override
    public void visit(Part part) {
        if (part instanceof CPU) {
            var cpu = (CPU) part;
            printf("CPU: %s (%.1f GHz)%n", cpu.name, cpu.GHz);
        } else if (part instanceof Memory) {
            var memory = (Memory) part;
            printf("RAM: %s (%d GB)%n", memory.type, memory.GB);
        } else if (part instanceof Fan) {
            var fan = (Fan) part;
            printf("CPU-Fan: %s%n", fan.name);
        } else if (part instanceof Screen) {
            var sc = (Screen) part;
            printf("Screen: %s %dx%d (%d Hz) %n",
                    sc.name, sc.resolutionWith, sc.resolutionHeight, sc.frequency);
        } else if (part instanceof Keyboard) {
            var kb = (Keyboard) part;
            printf("Keyboard: %s%n", kb.layout);
        } else if (part instanceof Touchpad) {
            var tp = (Touchpad) part;
            printf("Touchpad: %dx%d%n", tp.width, tp.height);
        }
    }

    @Override
    public void writeTo(Path file) {
        try (var out = Files.newBufferedWriter(file)) {
            out.write(buf.toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void printf(String format, Object... args) {
        String tab = "   ".repeat(currentIndent);
        out.printf(tab + format, args);
    }

}
