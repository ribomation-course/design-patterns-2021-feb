package ribomation.visitors;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ribomation.laptop.*;
import ribomation.parts.CompositePart;
import ribomation.parts.Part;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Stack;

public class XmlVisitor implements Visitor {
    Document doc;
    Element root;
    Stack<Element> nodes = new Stack<>();

    public XmlVisitor() {
        try {
            var bldr = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            doc = bldr.newDocument();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void visit(CompositePart part) {
        if (part instanceof Laptop) {
            var pc = (Laptop) part;
            var node = doc.createElement("laptop");
            node.setAttribute("name", pc.name);
            node.setAttribute("model", pc.model);
            nodes.push(node);
        } else if (part instanceof MotherBoard) {
            var mb = (MotherBoard) part;
            var node = doc.createElement("mother-board");
            node.setAttribute("model", mb.name);
            nodes.push(node);
        }
    }

    @Override
    public void visitEnd(CompositePart part) {
        var node = nodes.pop();
        if (nodes.empty()) {
            root = node;
        } else {
            nodes.peek().appendChild(node);
        }
    }

    @Override
    public void visit(Part part) {
        if (part instanceof CPU) {
            var cpu = (CPU) part;
            var tag = doc.createElement("cpu");
            add(tag, "name", cpu.name);
            add(tag, "frequency", cpu.GHz + " GHz");
            nodes.peek().appendChild(tag);
        } else
            if (part instanceof Memory) {
            var memory = (Memory) part;
            var tag = doc.createElement("memory");
            add(tag, "type", memory.type);
            add(tag, "size", memory.GB + " GB");
            nodes.peek().appendChild(tag);
        } else
            if (part instanceof Fan) {
            var fan = (Fan) part;
            var tag = doc.createElement("cpu-fan");
            add(tag, "name", fan.name);
            nodes.peek().appendChild(tag);
        } else
            if (part instanceof Screen) {
            var sc = (Screen) part;
            var tag = doc.createElement("screen");
            add(tag, "name", sc.name);
            add(tag, "resolution", sc.resolutionWith + "x" + sc.resolutionHeight);
            add(tag, "frequency", sc.frequency);
            nodes.peek().appendChild(tag);
        } else
            if (part instanceof Keyboard) {
            var kb = (Keyboard) part;
            var tag = doc.createElement("keyboard");
            add(tag, "layout", kb.layout);
            nodes.peek().appendChild(tag);
        } else
            if (part instanceof Touchpad) {
            var tp = (Touchpad) part;
            var tag = doc.createElement("touchpad");
            add(tag, "dimension", tp.width + "x" + tp.height + " mm");
            nodes.peek().appendChild(tag);
        }
    }

    private void add(Element parent, String name, double value) {
        add(parent, name, String.format("%.1f", value));
    }

    private void add(Element parent, String name, String value) {
        var childNode = doc.createElement(name);
        childNode.appendChild(doc.createTextNode(value));
        parent.appendChild(childNode);
    }

    public void writeTo(Path file) {
        doc.appendChild(root);
        try (var out = Files.newBufferedWriter(file)) {
            var transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            transformer.transform(new DOMSource(doc), new StreamResult(out));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}


