package ribomation.laptop;

import ribomation.parts.Part;

public class Memory implements Part {
    public final String type;
    public final int GB;
    public Memory(String type, int GB) {
        this.type = type;
        this.GB = GB;
    }
}
