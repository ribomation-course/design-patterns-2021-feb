package ribomation.laptop;

import ribomation.parts.Part;

public class CPU implements Part {
    public final String name;
    public final double GHz;
    public CPU(String name, double GHz) {
        this.name = name;
        this.GHz = GHz;
    }
}


