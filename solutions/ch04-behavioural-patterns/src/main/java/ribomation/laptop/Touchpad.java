package ribomation.laptop;

import ribomation.parts.Part;

public class Touchpad implements Part {
    public final int width;
    public final int height;
    public Touchpad(int width, int height) {
        this.width = width;
        this.height = height;
    }
}
