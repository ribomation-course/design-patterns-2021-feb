package ribomation;

import ribomation.api.Customer;
import ribomation.api.CustomerRepo;
import ribomation.api.Invoice;
import ribomation.api.Item;

import java.time.LocalDate;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    void run() {
        CustomerRepo.instance.addCustomers(repo -> {
            repo.addCustomer(new Customer("Anna Conda", "anna@gmail.com")
                    .addAddress(new Address("17 Reboot Lane", "88899", "ASM"))
                    .addInvoices(c -> {
                        c.addInvoice(new Invoice()
                                .addDate(LocalDate.of(2020, 12, 24))
                                .addAddress(new Address("42 Hacker Lane", "12345", "PWA"))
                                .addItems(i -> {
                                    i.addItem(new Item("Apple", 2, 1.25));
                                    i.addItem(new Item("Banana", 5, 1.75));
                                    i.addItem(new Item("Coco Nut", 2, 2.25));
                                }))
                                .addInvoice(new Invoice()
                                        .addDate(LocalDate.now())
                                        .addAddress(new Address("Östermalmstorg 1", "11442", "STH"))
                                        .addItems(i -> {
                                            i.addItem(new Item("Java for Rookies", 3, 123.45))
                                                    .addItem(new Item("Java Primer", 1, 456.78))
                                            ;
                                        })
                                )
                        ;
                    }));
        });

        System.out.println(CustomerRepo.instance.toString());
        System.out.printf("total: %.2f kr", CustomerRepo.instance.total());
    }
}
