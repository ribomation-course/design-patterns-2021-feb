package ribomation.api;

import ribomation.Address;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Invoice {
    LocalDate date = LocalDate.now();
    Optional<Address> deliveryAddress = Optional.empty();
    final List<Item> items = new ArrayList<>();

    public Invoice() {
        this.date = LocalDate.now();
    }

    public Invoice addDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public Invoice addAddress(Address addr) {
        deliveryAddress = Optional.ofNullable(addr);
        return this;
    }

    public Invoice addItem(Item item) {
        items.add(item);
        return this;
    }

    public Invoice addItems(Consumer<Invoice> stmts) {
        stmts.accept(this);
        return this;
    }

    public String toString() {
        return String.format("invoice: %s: %s = %.2f kr",
                date.toString(),
                items.stream()
                        .map(Item::toString)
                        .collect(Collectors.joining(", ")),
                total()
        );
    }

    public double total() {
        return items.stream()
                .mapToDouble(Item::total)
                .sum();
    }

}
