package ribomation.api;

import ribomation.Address;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

public class Customer {
    final String name;
    final String email;
    Optional<Address> invoiceAddress = Optional.empty();
    final List<Invoice> invoices = new ArrayList<>();

    public Customer(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public Customer addAddress(Address address) {
        this.invoiceAddress = Optional.of(address);
        return this;
    }

    public Customer addInvoice(Invoice invoice) {
        invoices.add(invoice);
        return this;
    }

    public Customer addInvoices(Consumer<Customer> stmts) {
        stmts.accept(this);
        return this;
    }

    @Override
    public String toString() {
        return String.format("Customer: %s <%s>, invoice-address: %s, invoices:%n%s%nTotal: %.2f kr%n",
                name, email, invoiceAddress,
                invoices.stream()
                        .map(Invoice::toString)
                        .collect(joining(System.lineSeparator())),
                total()
        );
    }

    public double total() {
        return invoices.stream()
                .mapToDouble(Invoice::total)
                .sum();
    }

}
