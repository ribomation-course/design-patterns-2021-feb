package ribomation.api;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

public class CustomerRepo {
    public static final CustomerRepo instance = new CustomerRepo();
    private final List<Customer> customers = new ArrayList<>();

    private CustomerRepo() {
    }

    public CustomerRepo addCustomer(Customer customer) {
        customers.add(customer);
        return this;
    }

    public CustomerRepo addCustomers(Consumer<CustomerRepo> stmts) {
        stmts.accept(this);
        return this;
    }

    @Override
    public String toString() {
        return String.format("Customer-repo: %n%s%n",
                customers.stream()
                        .map(Customer::toString)
                        .collect(joining(System.lineSeparator()))
        );
    }

    public double total() {
        return customers.stream().mapToDouble(Customer::total).sum();
    }
}
