package ribomation;

import java.nio.file.Path;
import java.util.concurrent.ForkJoinPool;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run(Path.of("../../../"));
    }

     void run(Path dir) {
         System.out.printf("Basedir: %s%n", dir.toAbsolutePath().normalize());

         var pool = new ForkJoinPool();
         var result = pool.invoke(new Task(dir));
         System.out.printf("Result: %s%n", result);
    }
}
