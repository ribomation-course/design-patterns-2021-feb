package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Counts {
    public int folders = 0;
    public int files = 0;
    public int bytes = 0;

    public Counts update(Path file) {
        try {
            ++files;
            bytes += Files.size(file);
            return this;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Counts merge(Counts that) {
        var result = new Counts();
        result.folders = this.folders + that.folders;
        result.files = this.files + that.files;
        result.bytes = this.bytes + that.bytes;
        return result;
    }

    @Override
    public String toString() {
        return String.format("folders=%d, files=%d, size=%.2f MB",
                folders, files, bytes / (1024.0 * 1024.0));
    }

}
