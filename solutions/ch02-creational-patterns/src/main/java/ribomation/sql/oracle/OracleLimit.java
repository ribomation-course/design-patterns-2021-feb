package ribomation.sql.oracle;

import ribomation.sql.Limit;

public class OracleLimit extends Limit {
    public OracleLimit(int maxNumValues) {
        super(maxNumValues);
    }

    @Override
    public String toSQL() {
        return String.format("ROWNUM <= %d", maxNumValues);
    }
}


