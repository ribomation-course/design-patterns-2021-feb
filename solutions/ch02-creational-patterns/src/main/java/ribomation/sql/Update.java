package ribomation.sql;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.joining;

public class Update implements SqlGenerator {
    private final String tableName;
    private final List<Set> columns = new ArrayList<>();
    private final List<Expression> where = new ArrayList<>();

    public Update(String tableName) {
        this.tableName = tableName;
    }

    public Update addColumn(Set set) {
        columns.add(set);
        return this;
    }

    public Update addExpression(Expression expr) {
        where.add(expr);
        return this;
    }

    @Override
    public String toSQL() {
        if (columns.isEmpty()) {
            throw new RuntimeException("Must have at least one SET col");
        }

        var columnsClause = columns.stream()
                .map(Set::toSQL)
                .collect(joining(", "));

        var whereClause = where.isEmpty() ? "" : where.stream()
                .map(Expression::toSQL)
                .collect(joining(" AND "));

        return String.format("UPDATE %s SET %s WHERE %s",
                tableName, columnsClause, whereClause);
    }
}
