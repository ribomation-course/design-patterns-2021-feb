package ribomation.sql;

public class Set implements SqlGenerator {
    private final String columnName;
    private final String value;

    public Set(String columnName, String value) {
        this.columnName = columnName;
        this.value = value;
    }

    @Override
    public String toSQL() {
        return String.format("%s = %s", columnName, value);
    }

}
