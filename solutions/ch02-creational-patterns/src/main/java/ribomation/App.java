package ribomation;

import org.yaml.snakeyaml.Yaml;
import ribomation.sql.SqlBuilder;
import ribomation.sql.mysql.MySqlBuilder;
import ribomation.sql.oracle.OracleSqlBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        app.run(args);
    }

    void run(String[] args) throws IOException {
        if (args.length > 0) {
            throw new RuntimeException("not support yet");
        } else {
            {
                Map<String, Object> data = new Yaml().load(this.getClass().getResourceAsStream("/persons-query.yml"));
                runQuery(data);
            }
            {
                Map<String, Object> data = new Yaml().load(this.getClass().getResourceAsStream("/persons-update.yml"));
                runUpdate(data);
            }
        }
    }

    void runQuery(Map<String, Object> data) {
        var builder = builder();
        var director = new QueryDirector(builder);
        var SQL = director.parse(data);
        System.out.printf("-- SQL --%n %s%n-- --- --%n", SQL);
    }

    void runUpdate(Map<String, Object> data) {
        var builder = builder();
        var director = new UpdateDirector(builder);
        var SQL = director.parse(data);
        System.out.printf("-- SQL --%n %s%n-- --- --%n", SQL);
    }

    SqlBuilder builder() {
        var dbType = databaseType();
        switch (dbType) {
            case "oracle":
                return new OracleSqlBuilder();
            case "mysql":
                return new MySqlBuilder();
        }
        throw new IllegalArgumentException("no such db type: " + dbType);
    }

    String databaseType() {
        var isOracle = Math.random() < .5;
        if (isOracle) return "oracle";
        return "mysql";
    }
}

