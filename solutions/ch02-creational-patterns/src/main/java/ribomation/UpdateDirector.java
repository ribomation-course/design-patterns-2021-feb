package ribomation;

import ribomation.sql.Expression;
import ribomation.sql.Set;
import ribomation.sql.SqlBuilder;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class UpdateDirector {
    private final SqlBuilder builder;

    public UpdateDirector(SqlBuilder builder) {
        this.builder = builder;
    }

    public String parse(Map<String, Object> dsl) {
        var update = builder.mkUpdate(tableName(dsl));

        columns(dsl).forEach(update::addColumn);
        expressions(dsl).forEach(update::addExpression);

        return update.toSQL();
    }

    protected String tableName(Map<String, Object> dsl) {
        return dsl.get("table").toString();
    }

    protected List<Set> columns(Map<String, Object> dsl) {
        var assignments = (List<Map<String, Object>>) dsl.get("set");
        if (assignments == null || assignments.isEmpty()) {
            throw new IllegalArgumentException("missing SET clause");
        }

        return assignments.stream()
                .map(expr -> {
                    var col = expr.get("col").toString();
                    var val = expr.get("val").toString();
                    return builder.mkSet(col, val);
                })
                .collect(toList());
    }

    protected List<Expression> expressions(Map<String, Object> dsl) {
        var exprLst = (List<Map<String, Object>>) dsl.get("where");
        if (exprLst == null) throw new IllegalArgumentException("missing where clause");
        return exprLst.stream()
                .map(expr -> {
                    var col = expr.get("col").toString();
                    var op = Expression.Operator.mk(expr.get("op").toString());
                    var val = expr.get("val");
                    return builder.mkExpression(col, op, val);
                })
                .collect(toList());
    }

}
