# Design Patterns 
### 2021 February

# Links
* [Installation Instructions](./installation-instructions.md)
* [Zoom Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md) 
* [Course Details](https://www.ribomation.se/courses/jvm/design-patterns-java)

# Usage of this GIT Repo
Ensure you have a [GIT client](https://git-scm.com/downloads) installed and clone this repo. 

    mkdir -p ~/design-patterns-course/my-solutions
    cd ~/design-patterns-course
    git clone <https url to this repo> gitlab

During the course, solutions will be push:ed to this repo and you can get these by a `git pull` operation

    cd ~/design-patterns-course/gitlab
    git pull

# Build Solution/Demo Programs
All programs are ordinary Java programs and can be compiled using any appropriate tool.
Several of the demo programs has a Gradle build file and can therefore be built by

    gradle build


Links to Large Files
====
For some of the exercises, you might want to use a large input file. Here are some compressed large text files to use.
* [English Text, 100MB (_38MB compressed_)](https://docs.ribomation.se/java/java-8/english.100MB.gz)
* [English Text, 1024MB (_396MB compressed_)](https://docs.ribomation.se/java/java-8/english.1024MB.gz)
* [Climate Data, 2,5GB (_432MB compressed_)](https://docs.ribomation.se/java/java-8/climate-data.txt.gz)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

