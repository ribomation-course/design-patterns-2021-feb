package ribomation.fork_join;

import ribomation.AppVariant;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

public class ForkJoin implements AppVariant {
    public static void main(String[] args) throws Exception {
        var filename = "./data/musketeers.txt";
        if (args.length > 0) filename = "./data/english.50MB.txt";

        new ForkJoin().run(filename, 25);
    }

    public void run(String filename, int maxWords) throws Exception {
        var path = Path.of(filename);
        var pool = new ForkJoinPool();

        var freqs = pool.invoke(new Task(path, 0, (int) Files.size(path)));
        freqs.entrySet().stream()
                .sorted((lhs, rhs) -> rhs.getValue() - lhs.getValue())
                .limit(maxWords)
                .forEach(e -> System.out.printf("%s: %s%n",
                        e.getKey(), e.getValue()));

        pool.shutdown();
        pool.awaitTermination(1, TimeUnit.SECONDS);
    }
}
