package ribomation;

public interface AppVariant {
    void run(String filename, int maxWords) throws Exception;
}
