package ribomation.thread_pool;

import ribomation.WordExtractor;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public class Task implements Callable<Map<String, Integer>> {
    private final Path path;
    private final int offset;
    private final int chunkSize;
    public static int MIN_WORD_SIZE = 5;

    public Task(Path path, long offset, long chunkSize) {
        this.path = path;
        this.offset = (int) offset;
        this.chunkSize = (int) chunkSize;
    }

    @Override
    public Map<String, Integer> call() {
        var freqs = new HashMap<String, Integer>();
        for (var word : new WordExtractor(path, offset, chunkSize)) {
            word = word.toLowerCase();
            if (word.length() >= MIN_WORD_SIZE) {
                freqs.put(word, 1 + freqs.getOrDefault(word, 0));
            }
        }
        return freqs;
    }

}


