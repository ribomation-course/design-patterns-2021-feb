package ribomation.pipeline;

import ribomation.AppVariant;

import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class Pipeline implements AppVariant {
    public static void main(String[] args) throws Exception {
        var filename = "./data/musketeers.txt";
        if (args.length > 0) filename = "./data/english.50MB.txt";

        new Pipeline().run(filename, 25);
    }

    public void run(String filename, int maxWords) throws Exception {
        var emitter    = new Emitter(maxWords);
        var sorter     = new Sorter(emitter);
        var aggregator = new Aggregator(sorter);
        var tokenizer  = new Tokenizer(aggregator);

        try (var in = Files.newBufferedReader(Path.of(filename), StandardCharsets.UTF_8)) {
            for (var line = readline(in); line != null; line = readline(in)) {
                tokenizer.send(line);
            }
        }
        tokenizer.send(null);
        tokenizer.join();
        aggregator.join();
        sorter.join();
        emitter.join();
    }

    private String readline(BufferedReader in) {
        try { return in.readLine();
        } catch (Exception e) { return ""; }
    }


}
