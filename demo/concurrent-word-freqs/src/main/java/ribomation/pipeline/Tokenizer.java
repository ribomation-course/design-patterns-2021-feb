package ribomation.pipeline;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Tokenizer extends ReceiverThread<String> {
    private final ReceiverThread<List<String>> next;

    public Tokenizer(ReceiverThread<List<String>> next) {
        this.next = next;
        start();
    }

    @Override
    public void run() {
        for (var line = recv(); line != null; line = recv()) {
            var batch = Stream.of(line.split("[^a-zA-Z]+"))
                    .map(String::toLowerCase)
                    .collect(toList());
            next.send(batch);
        }
        next.send(null);
    }
}
