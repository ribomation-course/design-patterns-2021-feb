package ribomation.pipeline;

import java.util.List;
import java.util.Map;

public class Emitter extends ReceiverThread<List<Map.Entry<String, Integer>>> {
    private final int max;

    public Emitter(int max) {
        this.max = max;
        start();
    }

    @Override
    public void run() {
        recv().stream().limit(max)
                .forEach(e -> System.out.printf("%s: %d%n",
                        e.getKey(), e.getValue()));
    }
}
