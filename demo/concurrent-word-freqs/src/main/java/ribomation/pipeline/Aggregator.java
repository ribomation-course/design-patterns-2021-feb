package ribomation.pipeline;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Aggregator extends ReceiverThread<List<String>> {
    public static int MIN_WORD_SIZE = 5;
    private final ReceiverThread<Map<String, Integer>> next;

    public Aggregator(ReceiverThread<Map<String, Integer>> next) {
        this.next = next;
        start();
    }

    @Override
    public void run() {
        var freqs = new HashMap<String, Integer>();
        for (var words = recv(); words != null; words = recv()) {
            words.forEach(word -> {
                if (word.length() >= MIN_WORD_SIZE) {
                    freqs.put(word, 1 + freqs.getOrDefault(word, 0));
                }
            });
        }
        next.send(freqs);
    }
}
