package ribomation;

import ribomation.fork_join.ForkJoin;
import ribomation.pipeline.Pipeline;
import ribomation.thread_pool.ThreadPool;

public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        app.parseArgs(args);
        app.run();
    }

    String filename = "./data/musketeers.txt";
    int maxWords = 5;
    void parseArgs(String[] args) {
        for (var k = 0; k < args.length; ++k) {
            switch (args[k]) {
                case "--file":
                    filename = args[++k];
                    break;
                case "--max":
                    maxWords = Integer.parseInt(args[++k]);
                    break;
                case "big":
                    filename = "./data/english.50MB.txt";
                    break;
            }
        }
    }

    void run() throws Exception {
        run("Thread Pool", new ThreadPool());
        run("Pipeline", new Pipeline());
        run("Fork-Join", new ForkJoin());
    }

    void run(String name, AppVariant variant) throws Exception {
        System.out.printf("--- %s ---%n", name);
        var startTime = System.nanoTime();
        variant.run(filename, maxWords);
        var endTime = System.nanoTime();
        System.out.printf("elapsed %.3f seconds%n", (endTime - startTime) * 1E-9);
    }
}


