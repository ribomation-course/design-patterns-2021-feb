package ribomation.java_advanced.atm;

public class SafeBankApp extends UnsafeBankApp {
    public static void main(String[] args) {
        new SafeBankApp().run(args);
    }
    
    @Override
    protected Account createAccount() {
        return new Account() {
            @Override
            public synchronized int getBalance() {
                return super.getBalance();
            }

            @Override
            public synchronized void updateBalance(int amount) {
                super.updateBalance(amount);
            }
        };
    }
}
