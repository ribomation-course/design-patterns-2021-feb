package ribomation.sql.oracle;

import ribomation.sql.*;
import ribomation.sql.mysql.MySqlBuilder;

public class OracleSqlBuilder extends MySqlBuilder {
    @Override
    public Limit mkLimit(int max) {
        return new OracleLimit(max);
    }
}


