package ribomation.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Query implements SqlGenerator {
    private final String tableName;
    private final List<String> columnNames = new ArrayList<>();
    private final List<Expression> where = new ArrayList<>();
    private Optional<OrderBy> sort = Optional.empty();
    private Optional<Limit> limit = Optional.empty();

    public Query(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public String toSQL() {
        var columns = columnNames.isEmpty() ? "*"
                : String.join(", ", columnNames);

        var whereClause = where.isEmpty() ? "" : where.stream()
                .map(Expression::toSQL)
                .collect(Collectors.joining(" AND "));

        var orderBy = sort.isEmpty() ? "" : sort.get().toSQL();

        var limitWith = limit.isEmpty() ? "" : limit.get().toSQL();

        return String.format("SELECT %s FROM %s WHERE %s %s %s",
                columns, tableName, whereClause, orderBy, limitWith)
                .trim();
    }

    public Query addColumn(String column) {
        columnNames.add(column);
        return this;
    }

    public Query addExpression(Expression expr) {
        where.add(expr);
        return this;
    }

    public Query orderBy(OrderBy by) {
        sort = Optional.of(by);
        return this;
    }

    public Query limit(Limit max) {
        limit = Optional.of(max);
        return this;
    }
}


