package ribomation;

import java.io.IOException;
import java.nio.file.Path;

import static java.lang.System.nanoTime;

public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        var dataFile = Path.of("../../../climate-data.txt.gz");
        app.run(dataFile);
    }

    void run(Path dataFile) throws IOException {
        var climateData = new ClimateData();

        var startTime = nanoTime();
        var yearlyAverages = climateData.computeYearlyAverages(dataFile);
        var endTime = nanoTime();

        System.out.printf("Year: AvgTemp%n");
        yearlyAverages.forEach(avg -> {
            System.out.printf("%d: %.2f C%n", avg.getKey(), avg.getValue());
        });

        System.out.printf("Elapsed %.3f seconds%n",
                (endTime - startTime) * 1E-9);
        System.out.printf("Processed: %.0f MB (%.0f MB compressed)%n",
                climateData.uncompressedByteCount() * 1E-6,
                climateData.compressedByteCount() * 1E-6
        );
    }
}



