package ribomation;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CountingInputStream extends FilterInputStream {
    private long byteCount = 0;
    protected CountingInputStream(InputStream in) { super(in); }
    public long getByteCount() { return byteCount; }
    public void reset() { byteCount = 0; }

    @Override
    public int read() throws IOException {
        ++byteCount;
        return super.read();
    }

    @Override
    public int read(byte[] b) throws IOException {
        var bytesRead = super.read(b);
        if (bytesRead > 0) byteCount += bytesRead;
        return bytesRead;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        var bytesRead = super.read(b, off, len);
        if (bytesRead > 0) byteCount += bytesRead;
        return bytesRead;
    }
}


