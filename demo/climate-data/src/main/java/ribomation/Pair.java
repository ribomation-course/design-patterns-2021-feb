package ribomation;

import java.util.AbstractMap;
import java.util.Map;

public class Pair {
    public static <First, Second>
    Map.Entry<First, Second>
    of(First first, Second second) {
        return new AbstractMap.SimpleEntry<>(first, second);
    }
}


