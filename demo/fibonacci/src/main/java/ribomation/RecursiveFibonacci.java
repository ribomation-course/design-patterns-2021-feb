package ribomation;

import java.math.BigInteger;

public class RecursiveFibonacci implements Fibonacci {
    @Override
    public BigInteger compute(int n) {
        if (n == 0) return BigInteger.ZERO;
        if (n == 1) return BigInteger.ONE;

        var f2 = compute(n - 2);
        var f1 = compute(n - 1);
        return f2.add(f1);
    }
}


