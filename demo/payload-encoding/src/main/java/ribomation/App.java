package ribomation;

import ribomation.encodings.CsvDispatcher;
import ribomation.encodings.JsonDispatcher;
import ribomation.encodings.XmlDispatcher;

import java.nio.file.Path;
import java.util.Map;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    void run() {
        var dispatchers = Map.of(
                "csv", new CsvDispatcher(),
                "xml", new XmlDispatcher(),
                "json", new JsonDispatcher()
        );

        var srcDir = Path.of("./src/main/resources/");
        var dstDir = Path.of("./tmp/");

        dispatchers.forEach((ext, dispatcher) -> {
            var infile  = Path.of(srcDir.toString(), "cars." + ext);
            var outfile = Path.of(dstDir.toString(), "cars." + ext);
            dispatcher.process(infile, outfile);
            System.out.printf("-- written %s%n", outfile);
        });
    }
}


