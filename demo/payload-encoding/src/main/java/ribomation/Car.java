package ribomation;

import java.util.StringJoiner;

public class Car {
    public final String maker;
    public final String model;
    public final Integer year;
    public final String country;

    public Car(String maker, String model, Integer year, String country) {
        this.maker = maker;
        this.model = model;
        this.year = year;
        this.country = country;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Car.class.getSimpleName() + "[", "]")
                .add("maker='" + maker + "'")
                .add("model='" + model + "'")
                .add("year=" + year)
                .add("city='" + country + "'")
                .toString();
    }
}
