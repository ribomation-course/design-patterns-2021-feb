package ribomation.encodings;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import ribomation.Car;
import ribomation.Dispatcher;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class XmlDispatcher extends Dispatcher {
    private final DocumentBuilder bldr;
    public XmlDispatcher() {
        try {
            bldr = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected List<Car> load(Path file) {
        try (var in = Files.newInputStream(file)) {
            var doc = bldr.parse(in);
            doc.getDocumentElement().normalize();

            var cars = new ArrayList<Car>();
            var carNodes = doc.getElementsByTagName("car");
            for (var k = 0; k < carNodes.getLength(); ++k) {
                var node = carNodes.item(k);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    var elem = (Element) node;
                    var maker = text(elem, "maker");
                    var model = text(elem, "model");
                    var year = text(elem, "year");
                    var country = text(elem, "country");
                    cars.add(new Car(maker, model, Integer.valueOf(year), country));
                }
            }
            return cars;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void store(Path file, List<Car> cars) {
        try (var out = Files.newBufferedWriter(file)) {
            var doc = bldr.newDocument();
            var root = doc.createElement("cars");
            doc.appendChild(root);

            cars.forEach(car -> {
                var node = doc.createElement("car");
                add(doc, node, "maker", car.maker);
                add(doc, node, "model", car.model);
                add(doc, node, "year", car.year.toString());
                add(doc, node, "country", car.country);
                root.appendChild(node);
            });

            var transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            transformer.transform(new DOMSource(doc), new StreamResult(out));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String text(Element elem, String name) {
        return elem.getElementsByTagName(name).item(0).getTextContent();
    }

    private void add(Document doc, Element parent, String name, String value) {
        var node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        parent.appendChild(node);
    }
}
