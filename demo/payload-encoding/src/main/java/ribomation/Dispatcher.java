package ribomation;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Dispatcher {
    public Dispatcher() { }

    public final void process(Path infile, Path outfile) {
        List<Car> cars = load(infile);

        cars = cars.stream()
                .filter(c -> c.year % 4 == 0)
                .map(c -> new Car(c.maker.toUpperCase(),
                        c.model.toLowerCase(),
                        c.year + 15,
                        c.country.toUpperCase()))
                .collect(Collectors.toList());

        cars.stream().limit(5).forEach(System.out::println);

        store(outfile, cars);
    }

    protected abstract List<Car> load(Path file);
    protected abstract void      store(Path file, List<Car> cars);
}



