package ribomation;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.function.Supplier;

public class CsvFileIteration implements Iterable<Person> {
    private final String filename;
    public CsvFileIteration(String filename) {
        this.filename = filename;
    }

    @Override
    public Iterator<Person> iterator() {
        return new Iterator<>() {
            private BufferedReader in;
            private String csvLine;
            private final Supplier<String> nextCsvLine = () -> {
                try { return in.readLine();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            };

            {
                try {
                    in = Files.newBufferedReader(Path.of(filename));
                    csvLine = nextCsvLine.get();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public boolean hasNext() { return csvLine != null; }

            @Override
            public Person next() {
                try { return Person.fromCSV(csvLine);
                } finally {
                    csvLine = nextCsvLine.get();
                }
            }
        };
    }

    public static void main(String[] args) {
        var n = 0;
        for (var p : new CsvFileIteration("./src/main/resources/persons.csv")) {
            if (n == 0) { ++n; continue; } //skip header
            System.out.printf("%d) %s%n", n++, p);
        }
    }
}


