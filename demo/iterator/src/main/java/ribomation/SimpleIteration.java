package ribomation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.rangeClosed;

public class SimpleIteration {
    public static void main(String[] args) {
        new SimpleIteration().run();
    }

    void run() {
        var array = new ArrayList<>(mk(10));
        iterateUsingWhile("array", array);
        iterateUsingForeach("array", array);

        var list = new LinkedList<>(mk(12));
        iterateUsingWhile("list", list);
        iterateUsingForeach("list", list);
    }

    void iterateUsingWhile(String label, List<Integer> number) {
        System.out.print(label + " (while): ");
        var it = number.iterator();
        while (it.hasNext()) System.out.printf("%d ", it.next());
        System.out.println();
    }

    void iterateUsingForeach(String label, List<Integer> number) {
        System.out.print(label + " (foreach): ");
        for (var num : number) System.out.printf("%d ", num);
        System.out.println();
    }

    List<Integer> mk(int n) {
        return rangeClosed(1, n).boxed().collect(toList());
    }
}



