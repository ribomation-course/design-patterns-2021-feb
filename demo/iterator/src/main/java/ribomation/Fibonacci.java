package ribomation;

import java.util.Iterator;

public class Fibonacci implements Iterable<Long> {
    private final int max;
    public Fibonacci(int max) { this.max = max; }

    @Override
    public Iterator<Long> iterator() {
        return new Iterator<>() {
            private long f2 = 0;
            private long f1 = 1;
            private int count = max;

            @Override
            public boolean hasNext() {
                return count > 0;
            }

            @Override
            public Long next() {
                var f = f2 + f1;
                f2 = f1;
                f1 = f;
                --count;
                return f2;
            }
        };
    }

    public static void main(String[] args) {
        var n = 1;
        for (var f : new Fibonacci(42)) {
            System.out.printf("fib(%d) = %d%n", n++, f);
        }
    }
}



