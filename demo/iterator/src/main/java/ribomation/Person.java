package ribomation;

public class Person {
    public String fname;
    public String lname;
    public String email;
    public String city;

    public Person(String fname, String lname, String email, String city) {
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.city = city;
    }

    public static Person fromCSV(String csv) {
        return fromCSV(csv, ";");
    }

    public static Person fromCSV(String csv, String sep) {
        var f = csv.split(sep);
        return new Person(f[0], f[1], f[2], f[3]);
    }

    @Override
    public String toString() {
        return String.format("%s %s, by %s in %s", fname, lname, email, city);
    }
}


