package ribomation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

import static javax.swing.JOptionPane.INFORMATION_MESSAGE;

public class Header extends JPanel {
    public Header() {
        super(new FlowLayout(FlowLayout.LEFT, 15, 0));

        var showMsg = new JButton(new AbstractAction("Show Message") {
            @Override
            public void actionPerformed(ActionEvent e) {
                var msg = "Just a silly message";
                var title = "Message";
                JOptionPane.showMessageDialog(
                        null, msg, title, INFORMATION_MESSAGE);
            }
        });

        var quit = new JButton(new AbstractAction("Quit App") {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        add(new JLabel("Actions:"));
        add(showMsg);
        add(quit);
    }
}
