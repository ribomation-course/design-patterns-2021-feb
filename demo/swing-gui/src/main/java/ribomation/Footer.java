package ribomation;

import javax.swing.*;
import java.awt.*;

public class Footer extends JPanel {
    public Footer() {
        super(new FlowLayout(FlowLayout.CENTER, 10, 0));
        add(new JLabel("Version 0.42"));
        add(new JLabel("-"));
        add(new JLabel("(c) TjollaHopp Software, Ltd."));
    }
}


