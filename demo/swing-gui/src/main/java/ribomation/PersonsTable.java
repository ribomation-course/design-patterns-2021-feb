package ribomation;

import javax.swing.*;
import java.awt.*;

import static java.awt.BorderLayout.CENTER;

public class PersonsTable extends JPanel {
    public PersonsTable(String personsResource) {
        super(new BorderLayout());
        var tbl = new JTable(new PersonsModel(personsResource));
        add(new JScrollPane(tbl), CENTER);
    }
}



