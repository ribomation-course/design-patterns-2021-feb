package ribomation;

import javax.swing.table.AbstractTableModel;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class PersonsModel extends AbstractTableModel {
    private final List<Person> persons;

    public PersonsModel(String resource) {
        var is = PersonsTable.class.getResourceAsStream(resource);
        var r = new BufferedReader(new InputStreamReader(is));
        try (r) {
            persons = r.lines().skip(1)
                    .map(Person::fromCSV)
                    .collect(toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int getRowCount() { return persons.size(); }
    @Override
    public int getColumnCount() { return 4; }

    @Override
    public Object getValueAt(int row, int column) {
        var p = persons.get(row);
        switch (column) {
            case 0: return p.fname;
            case 1: return p.lname;
            case 2: return p.email;
            case 3: return p.city;
        }
        throw new ArrayIndexOutOfBoundsException(column);
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0: return "First Name";
            case 1: return "Last Name";
            case 2: return "Email";
            case 3: return "City";
        }
        throw new ArrayIndexOutOfBoundsException(column);
    }
}



