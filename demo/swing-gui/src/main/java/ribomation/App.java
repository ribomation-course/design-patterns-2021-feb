package ribomation;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.setup();
        app.show();
    }

    GUI gui;
    void setup() {
        gui = new GUI("/persons.csv");
        gui.setup();
    }
    void show() {
        gui.pack();
        gui.setLocationByPlatform(true);
        gui.setVisible(true);
    }
}


