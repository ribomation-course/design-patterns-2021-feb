package ribomation.domain;

import ribomation.nano_spring.BeanFactory;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.init();
        app.run();
    }

    BeanFactory beans;

    void init() {
        beans = new BeanFactory("/beans.properties");
    }

    void run() {
        Person p = (Person) beans.getBean(Person.class);
        System.out.printf("person: %s%n", p);

        Cat c = (Cat) beans.getBean(Cat.class);
        c.setName("Ada");

        Engine e = (Engine) beans.getBean(Engine.class);
        e.setFuelType("Petrol");

        System.out.printf("person: %s%n", p);
    }
}
