package ribomation.gui;

import ribomation.commands.Command;

import javax.swing.*;

public class Header extends JToolBar {
    public Header() {
        add(new JLabel("Actions:"));
        add(Command.get("SillyMessage").newButton());
        addSeparator();
        add(Command.get("Quit").newButton());
    }
}


