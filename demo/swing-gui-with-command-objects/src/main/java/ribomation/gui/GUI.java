package ribomation.gui;

import ribomation.commands.Command;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static java.awt.BorderLayout.*;

public class GUI extends JFrame {
    public static GUI instance = new GUI();

    private GUI() throws HeadlessException {
        super("Simple Swing GUI");
        setLayout(new BorderLayout());

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Command.get("Quit").execute();
            }
        });
    }

    public void setup() {
        setJMenuBar(new AppMenubar());
        add(new Header(), NORTH);
        add(new PersonsTable(), CENTER);
        add(new Footer(), SOUTH);
    }
}


