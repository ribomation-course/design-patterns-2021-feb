package ribomation;

import ribomation.domain.PersonRepo;
import ribomation.gui.GUI;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.setup();
        app.show();
    }

    void setup() {
        PersonRepo.instance.load("/persons.csv");
        GUI.instance.setup();
    }
    void show() {
        GUI.instance.pack();
        GUI.instance.setLocationByPlatform(true);
        GUI.instance.setVisible(true);
    }
}


