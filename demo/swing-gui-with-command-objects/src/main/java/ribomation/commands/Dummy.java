package ribomation.commands;

import ribomation.gui.GUI;

import javax.swing.*;

public class Dummy extends Command {
    public Dummy() {
        setLabel("Dummy");
        setTooltip("Not implement yet");
        setIcon("howto");
    }

    @Override
    protected void doExecute() {
        JOptionPane.showMessageDialog(GUI.instance,
                "Not implemented yet",
                "Undefined Command",
                JOptionPane.INFORMATION_MESSAGE);
    }
}
