package ribomation.commands;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

public abstract class Command extends AbstractAction {
    private static final Map<String, Command> registry = new HashMap<>();

    public Command() {
        registry.put(getClass().getSimpleName().toLowerCase(), this);
    }

    public static Command get(String name) {
        var cmd = registry.get(name.toLowerCase());
        if (cmd != null) {
            return cmd;
        }

        cmd = load(name);
        if (cmd != null) {
            return cmd;
        }

        System.out.printf("** Command not found: %s%n", name);
        System.out.print("Loaded command names: ");
        registry.keySet().forEach(System.out::println);
        return new Dummy();
    }

    private static Command load(String name) {
        try {
            var clsName = String.format("%s.%s", Command.class.getPackageName(), name);
            return (Command) Class.forName(clsName).getDeclaredConstructor().newInstance();
        } catch (Exception x) {
            return null;
        }
    }

    @Override
    public final void actionPerformed(ActionEvent e) {
        doExecute();
    }
    public final void execute() {
        doExecute();
    }
    protected abstract void doExecute();

    protected void setLabel(String text) {
        putValue(Action.NAME, text);
    }
    protected void setMnemonic(char ch) {
        putValue(Action.MNEMONIC_KEY, (int) ch);
    }
    protected void setTooltip(String text) {
        putValue(Action.SHORT_DESCRIPTION, text);
    }

    protected void setIcon(String name) {
        var icon = loadIcon(name);
        putValue(Action.SMALL_ICON, icon);
        putValue(Action.LARGE_ICON_KEY, icon);
    }

    private ImageIcon loadIcon(String name) {
        var path = "/img/" + name + ".png";
        var url = getClass().getResource(path);
        if (url != null) {
            return new ImageIcon(url);
        }
        throw new IllegalArgumentException("icon not found: " + path);
    }

    public JButton newButton() {
        var b = new JButton();
        b.setVerticalTextPosition(SwingConstants.BOTTOM);
        b.setHorizontalTextPosition(SwingConstants.CENTER);
        b.setAction(this);
        return b;
    }

    public JMenuItem newMenuItem() {
        var mi = new JMenuItem();
        mi.setAction(this);
        return mi;
    }
}



