package ribomation.commands;

import ribomation.gui.GUI;

import javax.swing.*;

public class Quit extends Command {
    public Quit() {
        setLabel("Quit Application");
        setTooltip("Terminates this application");
        setIcon("quit");
        setMnemonic('Q');
    }

    @Override
    protected void doExecute() {
        var rc = JOptionPane.showConfirmDialog(GUI.instance,
                "Do you really want to quit?",
                "Quit?",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (rc == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }
}

