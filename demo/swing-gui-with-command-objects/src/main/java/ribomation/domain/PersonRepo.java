package ribomation.domain;

import ribomation.gui.PersonsTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class PersonRepo {
    public static final PersonRepo instance = new PersonRepo();
    private List<Person> persons = new ArrayList<>();
    private PersonRepo() { }
    public List<Person> getPersons() { return persons; }

    public void load(String resource) {
        var is = PersonsTable.class.getResourceAsStream(resource);
        var r = new BufferedReader(new InputStreamReader(is));
        try (r) {
            persons = r.lines().skip(1)
                    .map(Person::fromCSV)
                    .collect(toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}


