package ribomation.java_advanced.swift;

public abstract class App {
    private int numTransfers = 500_000;
    private SendingBank   sendingBank;
    private ReceivingBank receivingBank;

    protected abstract MoneyBox createMoneyBox();

    protected void run(String[] args) {
        parseArgs(args);
        setup();
        transfers();
        summarize();
    }

    private void parseArgs(String[] args) {
        for (int k = 0; k < args.length; k++) {
            if (args[k].equals("-t")) {
                numTransfers = Integer.parseInt(args[++k]);
            }
        }
        System.out.printf("[bank] # transfers = %d%n", numTransfers);
    }

    private void setup() {
        MoneyBox moneyBox = createMoneyBox();
        receivingBank = new ReceivingBank(moneyBox);
        sendingBank = new SendingBank(moneyBox, numTransfers);
    }

    private void transfers() {
        receivingBank.start();
        sendingBank.start();
        System.out.printf("[bank] sending money...%n");
        try { sendingBank.join(); } catch (InterruptedException ignore) { }
        try { receivingBank.join(); } catch (InterruptedException ignore) { }
    }

    private void summarize() {
        System.out.printf("[bank] %s%n", sendingBank);
        System.out.printf("[bank] %s%n", receivingBank);
    }
}
