package ribomation.java_advanced.swift;

public class UnsafeSwiftApp extends App {
    public static void main(String[] args) {
        new UnsafeSwiftApp().run(args);
    }

    @Override
    protected MoneyBox createMoneyBox() {
        return new MoneyBox();
    }
}
