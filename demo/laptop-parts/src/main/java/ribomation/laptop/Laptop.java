package ribomation.laptop;

import ribomation.parts.CompositePart;

public class Laptop extends CompositePart {
    public final String name;
    public final String model;
    public Laptop(String name, String model) {
        this.name = name;
        this.model = model;
    }
}


