package ribomation.visitors;

import ribomation.parts.CompositePart;
import ribomation.parts.Part;

public interface Visitor {
    void visit(CompositePart part);
    void visitEnd(CompositePart part);
    void visit(Part part);
}


