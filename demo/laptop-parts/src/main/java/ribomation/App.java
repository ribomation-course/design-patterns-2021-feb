package ribomation;

import ribomation.laptop.*;
import ribomation.parts.Part;
import ribomation.visitors.DumpVisitor;
import ribomation.visitors.XmlVisitor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class App {
    public static void main(String[] args) throws IOException {
        new App().run();
    }

    void run() throws IOException {
        var flipper = build();
        dump(flipper);
        xml(flipper, Path.of("./tmp/laptop.xml"));
    }

    Part build() {
        return new Laptop("Dell Inspiron", "7773 2-in-1")
                .with(p -> {
                    p.add(new Screen("Philips BDM3470", 3440, 1440, 29));
                    p.add(new Keyboard("English GB-en"));
                    p.add(new Touchpad(105, 80));
                    p.add(new MotherBoard("Dell 0R58C3")
                            .with(mb -> {
                                mb.add(new CPU("Intel Core i7", 2.9));
                                mb.add(new Memory("DDR4 2400MHz", 16));
                                mb.add(new Fan("M3800 Cooling Fan"));
                            }));
                });
    }

    void dump(Part laptop) {
        System.out.printf("-- dump --%n");
        laptop.accept(new DumpVisitor());
    }

    void xml(Part laptop, Path file) throws IOException {
        System.out.printf("-- XML --%n");
        var tmpDir = file.getParent();
        Files.createDirectories(tmpDir);

        var xml = new XmlVisitor();
        laptop.accept(xml);
        xml.writeTo(file);
        System.out.printf("written %s (%d bytes)%n", file, Files.size(file));
    }
}



