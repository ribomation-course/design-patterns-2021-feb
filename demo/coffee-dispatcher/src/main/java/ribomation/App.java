package ribomation;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        var app = new App();
        var items = app.populate(1_000_000);
        app.dump(items);
    }

    ArrayList<CoffeeFlavour> populate(int N) {
        var items = new ArrayList<CoffeeFlavour>();
        while (N-- > 0) {
            items.add(CoffeeFlavourGenerator.instance.generate());
        }
        return items;
    }

    void dump(ArrayList<CoffeeFlavour> items) {
        final var chunk = 5;
        final int[] itemNo = {1};
        items.stream().limit(chunk)
                .forEach(item -> System.out.printf("%d) %s%n", itemNo[0]++, item));
        System.out.printf("  . . . %n");
        itemNo[0] = items.size() - chunk + 1;
        items.stream().skip(items.size() - chunk)
                .forEach(item -> System.out.printf("%d) %s%n", itemNo[0]++, item));

        System.out.printf("# items  = %,d%n", items.size());
        System.out.printf("# cached = %,d%n", CoffeeFlavour.cacheSize());
    }
}


