package ribomation;

import ribomation.CoffeeFlavour.Cream;
import ribomation.CoffeeFlavour.Sugar;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class CoffeeFlavourGenerator {
    public static final CoffeeFlavourGenerator instance = new CoffeeFlavourGenerator();
    private CoffeeFlavourGenerator() { }

    public CoffeeFlavour generate() {
        var name = names.get(r.nextInt(names.size()));
        var cream = r.nextBoolean() ? null : (r.nextBoolean() ? Cream.dash : Cream.tablespoon);
        var sugar = r.nextBoolean() ? null : (r.nextBoolean() ? Sugar.pinch : Sugar.teaspoon);
        return CoffeeFlavour.get(name, cream, sugar);
    }

    private final Random r = new Random();
    private final List<String> names = Arrays.asList(
            "Americano", "Cappuccino", "Espresso", "Mocha"
    );
}



